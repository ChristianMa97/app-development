package de.hdmstuttgart.movietracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity{
    private static RecyclerView recyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    List<MovieView> list = new ArrayList();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list.add(new MovieView("Dr. No", "1962", "Sean Connery"));
        list.add(new MovieView("From Russia with Love", "1963", "Sean Connery"));
        list.add(new MovieView("Goldfinger", "1964", "Sean Connery"));
        list.add(new MovieView("Thunderball", "1965", "Sean Connery"));
        list.add(new MovieView("You Only Live Twice", "1967", "Sean Connery"));
        recyclerView = (RecyclerView) findViewById(R.id.homeRecyclerView);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        mAdapter = new MyAdapter(getContentList());
        recyclerView.setAdapter(mAdapter);
        mAdapter.setmListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                list.remove(position);
                mAdapter.notifyItemRemoved(position);
            }
        });
        //

        recyclerView.setLayoutManager(layoutManager);
    }

    public List getContentList(){
        return list;
    }
}
