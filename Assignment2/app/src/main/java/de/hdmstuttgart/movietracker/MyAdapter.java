package de.hdmstuttgart.movietracker;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.selection.SelectionTracker;
import androidx.recyclerview.selection.StableIdKeyProvider;
import androidx.recyclerview.selection.StorageStrategy;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private List<MovieView> mDataset;
    private OnItemClickListener mListener;

    //Listener
    public interface OnItemClickListener{
        void onItemClick(int position);
    }

    public void setmListener(OnItemClickListener listener){
        mListener=listener;
    }

    public MyAdapter(List myDataset) {
        mDataset=myDataset;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movieview, parent, false);

        MyViewHolder vh = new MyViewHolder(v, mListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.title.setText(mDataset.get(position).title);
        holder.year.setText(mDataset.get(position).year);
        holder.actor.setText(mDataset.get(position).actor);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView year;
        public TextView actor;


        public MyViewHolder(View v, final OnItemClickListener listener) {
            super(v);
            title=v.findViewById(R.id.title);
            year=v.findViewById(R.id.year);
            actor=v.findViewById(R.id.actor);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener!=null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }

    }



}
