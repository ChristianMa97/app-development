package de.hdmstuttgart.movietracker;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.TextView;

public class MovieView {

    String title;
    String year;
    String actor;

    public MovieView(){

    }
    public MovieView(String title, String year, String actor){
        this.title=title;
        this.year=year;
        this.actor=actor;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }
}
