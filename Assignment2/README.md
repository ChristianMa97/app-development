# Assignment 2

<br>

## Tasks

* Add a vertical RecyclerView to your MainActivity with “android:id=“@+id/homeRecyclerView””
* Create item views as described
* When user clicks on a row, remove the row
 
<br>

![ItemViews](pictures/Item Views.png)
![RowData](pictures/RowData.png)
