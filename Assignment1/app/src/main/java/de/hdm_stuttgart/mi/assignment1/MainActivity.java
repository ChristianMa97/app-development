package de.hdm_stuttgart.mi.assignment1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button myButton;
    TextView textView;
    private int counter=0;
    public final static String EXTRA_MESSAGE = "de.hdm_stuttgart.counter"; //key for Intent

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myButton = (Button)findViewById(R.id.button);   //Get instance of button
        textView = (TextView)findViewById(R.id.text);   //Get instance of textview

    }

    /**
     * Eventhandler for button
     */
    public void handleButton(View view) {
        counter++;
        if(counter<5) {
            textView.setText("You clicked " + counter+" times");
        }else{
            //Create intent for Main2Activity
            Intent intent = new Intent(this, Main2Activity.class);
            //Add the content of counter to the intent
            intent.putExtra(EXTRA_MESSAGE, ""+counter);

            startActivity(intent);

        }
    }
}
