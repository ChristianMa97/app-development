# Assignment 1
<br>

## Tasks

- Create a new Android Project in Android Studio
- Create an AVD
- Create an UI with a TextView and a Button
- Debug your app
<br>

When the button is touched, the text changes correspondingly.

When user clicked 5 times on a button, open a new activity and show the value of the counting variable.
