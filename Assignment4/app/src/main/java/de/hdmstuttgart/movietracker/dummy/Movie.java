package de.hdmstuttgart.movietracker.dummy;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import de.hdmstuttgart.movietracker.ItemListActivity;

public class Movie implements Serializable {

    private static final List<Movie> favoriteMovies = new ArrayList<>();

    public static List<Movie> getFavoriteMovies(){
        return favoriteMovies;
    }

    public static void addToFavorite(Movie movie){
        favoriteMovies.add(movie);
        System.out.println("Item added to favorite: "+ movie.Title);
        ItemListActivity.adapter.notifyDataSetChanged();
    }
    public static void removeFromFavorite(Movie movie){
        favoriteMovies.remove(movie);
        System.out.println("Item removed from favorite: "+movie.Title);
        ItemListActivity.adapter.notifyDataSetChanged();
    }

    public String Title;
    public String Year;
    public String imdbID;
    public String Type;
    public String Poster;
}