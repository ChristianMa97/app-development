package de.hdmstuttgart.movietracker.dummy;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieApi {

    @GET(".")
    Call<SearchResponse> getSearchResult(@Query("s") String movieSearch, @Query("apikey") String apiKey);
}