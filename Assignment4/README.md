# Assignment 4

## Tasks:
* Copy your project from Assignment3
* Go to https://www.omdbapi.com/apikey.aspx and create your private API key for OMDb API “FREE! (1,000 daily limit)”
* Don’t forgett to add Internet Permission to your manifest.
* Show empty ItemListActivity on app start by removing your static test data from Assignment3
* Extend your SearchActivity

	- When user enters title in EditText and hits Search Button, initialize Retrofit with GSON and query the API as described in the lecture slides
	- Add necessary Retrofit classes to your project
	- While API is loading, show the user a circular ProgressBar https://developer.android.com/reference/android/widget/ProgressBar
	- Hide ProgressBar when API finished loading
	- When user enters “Goldfinger” in EditText and hits Search Button, “James Bond 007: Goldfinger” should be your second search result.
- Only smartphone view is considered for testing
