# Assignment 5

<br>

## Tasks:

* Create a new folder “Assignment5” in your git repo.
* Copy your project from Assignment4
* Add Glide and Room to your app Gradle (see Gradle Dependencies)
* Add an ImageView with id R.id.poster to your ItemDetailActivity, SearchActivity and ItemListActivity.
* Use Glide to load and show the poster image to the selected movie entries.
* Setup MovieDao and AppDatabase as shown in the lecture to persist movie entries.
* Extend your implementation to persist saved movies across app starts.
