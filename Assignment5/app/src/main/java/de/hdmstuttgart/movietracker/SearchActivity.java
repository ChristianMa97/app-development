package de.hdmstuttgart.movietracker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import de.hdmstuttgart.movietracker.dummy.Movie;
import de.hdmstuttgart.movietracker.dummy.MovieApi;
import de.hdmstuttgart.movietracker.dummy.SearchResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchActivity extends AppCompatActivity {
    private ItemListActivity.SimpleItemRecyclerViewAdapter adapter;
    private EditText editText;
    protected List<Movie> movieList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        View recyclerView = findViewById(R.id.searchList);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
        editText= findViewById(R.id.searchInput);
        Button searchButton = findViewById(R.id.searchBtn);

        //When the search button is pressed the list will be updated to the new context.
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().length() < 1) {
                    editText.setText("Fuck You");
                } else {
                    System.out.println("search clicked");
                    movieList.clear();
                    adapter.notifyDataSetChanged();
                    findViewById(R.id.progress).setVisibility(View.VISIBLE);


                    Gson gson = new GsonBuilder()
                            .setLenient()
                            .create();

                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(Constants.BaseURL)
                            .addConverterFactory(GsonConverterFactory.create(gson))
                            .build();

                    MovieApi movieApi = retrofit.create(MovieApi.class);

                    String searchString = editText.getText().toString();

                    Call<SearchResponse> call = movieApi.getSearchResult(searchString, Constants.Key);

                    call.enqueue(new Callback<SearchResponse>() {
                        @Override
                        public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                            if(response.body().Search!=null){
                            movieList.addAll(response.body().Search);
                            adapter.notifyDataSetChanged();
                            findViewById(R.id.progress).setVisibility(View.GONE);
                            }else{
                                editText.setText("Nothing Found");
                                findViewById(R.id.progress).setVisibility(View.GONE);

                            }
                        }

                        @Override
                        public void onFailure(Call<SearchResponse> call, Throwable t) {
                            Log.e("Oh noo.", t.getMessage());
                            findViewById(R.id.progress).setVisibility(View.GONE);

                        }
                    });
                }
            }
            });
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView){
        adapter= new ItemListActivity.SimpleItemRecyclerViewAdapter(this, movieList, false);
        recyclerView.setAdapter(adapter);
    }
}
