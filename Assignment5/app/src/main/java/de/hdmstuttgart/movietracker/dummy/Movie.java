package de.hdmstuttgart.movietracker.dummy;

import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;




@Entity
public class Movie implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int uid;
    public String Title;
    public String Year;
    public String imdbID;
    public String Type;
    public String Poster;

    @Override
    public int hashCode() {
        return imdbID.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj instanceof Movie){
            Movie second = (Movie) obj;
            return imdbID.equals(second.imdbID);
        }
        return false;
    }
}