package de.hdmstuttgart.movietracker;

import android.app.Activity;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdmstuttgart.movietracker.dummy.AppDatabase;
import de.hdmstuttgart.movietracker.dummy.Movie;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link ItemListActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    private static final String TAG = "ItemDetailFragment";
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_TITLE = "item_title";
    public static final String ARG_MOVIE = "movie_item";

    /**
     * The dummy content this fragment is presenting.
     */
    private Movie mItem;
    List <Movie> movieDetailList;


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_TITLE)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            //mItem = DummyContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_TITLE));
            mItem = (Movie)getArguments().get(ItemDetailFragment.ARG_MOVIE);

            Activity activity = this.getActivity();
            Button deleteButton = activity.findViewById(R.id.favoriteButton);
            if(deleteButton!=null){
            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeFromFavorite(mItem);
                }
            });}

            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.Title);
            }
            FloatingActionButton fab = (FloatingActionButton)activity.findViewById(R.id.fab);
            if(fab!=null){
                if(getFavorites().contains(mItem)){
                    fab.setImageResource(android.R.drawable.ic_menu_delete);
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            removeFromFavorite(mItem);
                        }
                    });
                }else{
                    fab.setImageResource(android.R.drawable.ic_menu_add);
                    fab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            addToFavorite(mItem);
                        }
                    });
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            Glide.with(this).load(mItem.Poster).into((ImageView)rootView.findViewById(R.id.poster));
            ((TextView) rootView.findViewById(R.id.title)).setText(mItem.Title);
            ((TextView) rootView.findViewById(R.id.year)).setText(mItem.Year);
            ((TextView) rootView.findViewById(R.id.imdbID)).setText(mItem.imdbID);
            ((TextView) rootView.findViewById(R.id.type)).setText(mItem.Type);
        }

        return rootView;
    }

    private void addToFavorite(final Movie mItem){
        new Thread(new Runnable() {
            @Override
            public void run() {
                AppDatabase db = Room.databaseBuilder(getContext().getApplicationContext(),
                        AppDatabase.class, "movieDb").build();

                db.movieDetailDao().insert(mItem);
                Log.d(TAG, "added Item to Favorites: "+ mItem.Title);
            }
        }).start();
    }
    private void removeFromFavorite(final Movie mItem){

        new Thread(new Runnable() {
            @Override
            public void run() {
                AppDatabase db = Room.databaseBuilder(getContext().getApplicationContext(),
                        AppDatabase.class, "movieDb").build();

                db.movieDetailDao().delete(mItem);
            }
        }).start();
    }
    public List<Movie> getFavorites(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                AppDatabase db = Room.databaseBuilder(getContext().getApplicationContext(),
                        AppDatabase.class, "movieDb").build();

                movieDetailList = db.movieDetailDao().getAll();
            }
        });
        thread.start();
        while (thread.isAlive()){
            System.out.println("Thread is alive.. waiting for database call");
        }
        System.out.println("Got favorite_List with content"+ movieDetailList);
        return movieDetailList;
    }
}
