# Assignment 3

<br>

## Tasks

### Add the folowing Activities with their fragments and content:

* **ItemListActivity**

	- Add Fragment to show saved movies (favorites) in a RecyclerView
	- Use Master - Detail - Flow to show different view between smartphone and tablet devices
	- Add floating action button and navigate to SearchActivity when clicked by user https://developer.android.com/guide/topics/ui/floating-action-button
	- On tablet view, add Button delete favorite in right page detail. When button is pressed, update left pannel list view by removing this movie.
	- IDs:
		- FAB: R.id.fab_search
		- RecyclerView: R.id.movieList
		- List row ids same as Assignment2 (R.id.title, R.id.year, R.id.actor)
		- Tablet detail remove button: R.id.favoriteButton
	- On app start, show single row with “Dr. No” data from Assignment2

* **ItemDetailActivity**

	- Fab button to save or remove favorite movies
	- IDs:
		- FAB: R.id.fab
		- Detail information same as Assignment2 (TextView with R.id.title, R.id.year, aR.id.ctor)

* **SearchActivity**

	- Show EditText and Button to search for movie titles
	- Use movies from Assignment 2 / Row Data as source
	- When user enters title in EditText and hits Search Button, show result as RecyclerView
	- Search in movie title for user input contains. Example: Input “er” should show 2 result rows Goldfinger and Thunderball.
	- When user clicks on result entry, show detail activity with the selected moview
	- IDs:
		- EditText: R.id.searchInput
		- Button: R.id.searchBtn
		- RecyclerView: R.id.searchList
		- List row ids same as Assignment2 (R.id.title, R.id.year, R.id.actor)
<br>

### Views:

![ItemDetailActivity](pictures/ItemDetailActivity.png)
![ItemListActivity](pictures/ItemListActivity.png)
![SearchActivity](pictures/SearchActivity.png)
