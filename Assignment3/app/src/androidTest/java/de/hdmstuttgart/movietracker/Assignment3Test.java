package de.hdmstuttgart.movietracker;


import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class Assignment3Test {

    @Rule
    public ActivityTestRule<ItemListActivity> mActivityTestRule = new ActivityTestRule<>(ItemListActivity.class);

    @Test
    public void assignment3Test() {
        ViewInteraction textView = onView(
                allOf(withId(R.id.title), withText("Dr. No"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.movieList),
                                        0),
                                0),
                        isDisplayed()));
        textView.check(matches(withText("Dr. No")));

        textView.perform(click());

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.title), withText("Dr. No"),
                        isDisplayed()));
        textView2.check(matches(withText("Dr. No")));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.year), withText("1962"),
                        isDisplayed()));
        textView3.check(matches(withText("1962")));

        ViewInteraction textView4 = onView(
                allOf(withId(R.id.actor), withText("Sean Connery"),
                        isDisplayed()));
        textView4.check(matches(withText("Sean Connery")));

        pressBack();

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.fab_search),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.searchInput),
                        isDisplayed()));
        appCompatEditText.perform(replaceText("rus"), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.searchBtn), withText("search"),
                        isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction textView5 = onView(
                allOf(withId(R.id.title), withText("From Russia with Love"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.searchList),
                                        0),
                                0),
                        isDisplayed()));
        textView5.check(matches(withText("From Russia with Love")));

        textView5.perform(click());

        ViewInteraction textView6 = onView(
                allOf(withId(R.id.title), withText("From Russia with Love"),
                        isDisplayed()));
        textView6.check(matches(withText("From Russia with Love")));

        pressBack();

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.searchInput), withText("rus"),
                        isDisplayed()));
        appCompatEditText2.perform(click());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.searchInput), withText("rus"),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("thun"));

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.searchInput), withText("thun"),
                        isDisplayed()));
        appCompatEditText4.perform(closeSoftKeyboard());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.searchBtn), withText("search"),
                        isDisplayed()));
        appCompatButton2.perform(click());

        ViewInteraction textView7 = onView(
                allOf(withId(R.id.title), withText("Thunderball"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.searchList),
                                        0),
                                0),
                        isDisplayed()));
        textView7.check(matches(withText("Thunderball")));

        textView7.perform(click());

        ViewInteraction textView8 = onView(
                allOf(withId(R.id.title), withText("Thunderball"),
                        isDisplayed()));
        textView8.check(matches(withText("Thunderball")));

        ViewInteraction floatingActionButton2 = onView(
                allOf(withId(R.id.fab),
                        isDisplayed()));
        floatingActionButton2.perform(click());

        pressBack();
        pressBack();

        ViewInteraction textView9 = onView(
                allOf(withId(R.id.title), withText("Thunderball"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.movieList),
                                        1),
                                0),
                        isDisplayed()));
        textView9.check(matches(withText("Thunderball")));

        ViewInteraction floatingActionButton3 = onView(
                allOf(withId(R.id.fab_search),
                        isDisplayed()));
        floatingActionButton3.perform(click());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.searchInput),
                        isDisplayed()));
        appCompatEditText5.perform(click());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.searchInput),
                        isDisplayed()));
        appCompatEditText6.perform(replaceText("gold"), closeSoftKeyboard());

        ViewInteraction appCompatButton3 = onView(
                allOf(withId(R.id.searchBtn), withText("search"),
                        isDisplayed()));
        appCompatButton3.perform(click());

        ViewInteraction textViewGold = onView(
                allOf(withId(R.id.title), withText("Goldfinger"),
                        isDisplayed()));

        textViewGold.perform(click());

        ViewInteraction floatingActionButton4 = onView(
                allOf(withId(R.id.fab),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        floatingActionButton4.perform(click());

        pressBack();

        ViewInteraction textView10 = onView(
                allOf(withId(R.id.title), withText("Goldfinger"),
                        isDisplayed()));
        textView10.check(matches(withText("Goldfinger")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    public static class RecyclerViewMatcher {
        private final int recyclerViewId;

        public RecyclerViewMatcher(int recyclerViewId) {
            this.recyclerViewId = recyclerViewId;
        }

        public Matcher<View> atPosition(final int position) {
            return atPositionOnView(position, -1);
        }

        public Matcher<View> atPositionOnView(final int position, final int targetViewId) {

            return new TypeSafeMatcher<View>() {
                Resources resources = null;
                View childView;

                public void describeTo(Description description) {
                    String idDescription = Integer.toString(recyclerViewId);
                    if (this.resources != null) {
                        try {
                            idDescription = this.resources.getResourceName(recyclerViewId);
                        } catch (Resources.NotFoundException var4) {
                            idDescription = String.format("%s (resource name not found)",
                                    new Object[] { Integer.valueOf
                                            (recyclerViewId) });
                        }
                    }

                    description.appendText("with id: " + idDescription);
                }

                public boolean matchesSafely(View view) {

                    this.resources = view.getResources();

                    if (childView == null) {
                        RecyclerView recyclerView =
                                (RecyclerView) view.getRootView().findViewById(recyclerViewId);
                        if (recyclerView != null && recyclerView.getId() == recyclerViewId) {
                            childView = recyclerView.findViewHolderForAdapterPosition(position).itemView;
                        }
                        else {
                            return false;
                        }
                    }

                    if (targetViewId == -1) {
                        return view == childView;
                    } else {
                        View targetView = childView.findViewById(targetViewId);
                        return view == targetView;
                    }

                }
            };
        }
    }

    public static RecyclerViewMatcher withRecyclerView(final int recyclerViewId) {

        return new RecyclerViewMatcher(recyclerViewId);
    }
}
