package de.hdmstuttgart.movietracker.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdmstuttgart.movietracker.ItemListActivity;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();
    public static final List<DummyItem> FAVORITE_ITEMS = new ArrayList<DummyItem>();


    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.title, item);
    }

    public static void addToFavorite(String title){
        FAVORITE_ITEMS.add(ITEM_MAP.get(title));
        System.out.println("Item added to favorite: "+ title);
        ItemListActivity.adapter.notifyDataSetChanged();
    }
    public static void removeFromFavorite(String title){
        DummyItem item = ITEM_MAP.get(title);
        FAVORITE_ITEMS.remove(item);
        System.out.println("Item removed from favorite: "+title);
        ItemListActivity.adapter.notifyDataSetChanged();
    }

    public static List<DummyItem> searchItems(String searchString){
        System.out.println("searchList called with String: "+searchString);
        List<DummyItem> resultList = new ArrayList<DummyItem>();
        for (DummyItem item: ITEMS) {
            if (item.title.toLowerCase().contains(searchString.toLowerCase())){
                resultList.add(item);
            }
        }
        return resultList;
    }

    static{
        addItem(new DummyItem("Dr. No","1962","Sean Connery"));
        addItem(new DummyItem("From Russia with Love","1963","Sean Connery"));
        addItem(new DummyItem("Goldfinger","1964","Sean Connery"));
        addItem(new DummyItem("Thunderball","1965","Sean Connery"));
        addItem(new DummyItem("You Only Live Twice","1967","Sean Connery"));
        FAVORITE_ITEMS.add(ITEM_MAP.get("Dr. No"));
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String title;
        public final String year;
        public final String actor;

        public DummyItem(String title, String year, String actor) {
            this.title = title;
            this.year = year;
            this.actor = actor;
        }

        @Override
        public String toString() {
            return title;
        }
    }
}
