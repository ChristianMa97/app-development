package de.hdmstuttgart.movietracker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import de.hdmstuttgart.movietracker.dummy.DummyContent;

public class SearchActivity extends AppCompatActivity {
    private ItemListActivity.SimpleItemRecyclerViewAdapter adapter;
    private final List<DummyContent.DummyItem> searchList = new ArrayList<>();
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        View recyclerView = findViewById(R.id.searchList);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
        editText= findViewById(R.id.searchInput);
        Button searchButton = findViewById(R.id.searchBtn);

        //When the search button is pressed the list will be updated to the new context.
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("search clicked");
                searchList.clear();
                searchList.addAll(DummyContent.searchItems(editText.getText().toString()));
                for (DummyContent.DummyItem i: searchList) {
                    System.out.println(i.title);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView){
        adapter= new ItemListActivity.SimpleItemRecyclerViewAdapter(this, searchList, false);
        recyclerView.setAdapter(adapter);
    }
}
